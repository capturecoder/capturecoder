
### Hi there, I'm Nitish -  [capturecoder][website] 👋


[![Website](https://img.shields.io/website?label=Nitish_Gupta&style=for-the-badge&url=https://capturecoder.github.io/)](https://capturecoder.github.io/)


## I'm a New Developer

### Connect with me:

[<img align="left" alt="Nitish Gupta portfolio" width="22px" src="https://raw.githubusercontent.com/iconic/open-iconic/master/svg/globe.svg" />][website]
[<img align="left" alt="capturecoder| Facebook" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/facebook.svg" />][facebook]
[<img align="left" alt="capturecoder | YouTube" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/youtube.svg" />][youtube]
[<img align="left" alt="capturecoder | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />][twitter]
[<img align="left" alt="capturecoder | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />][linkedin]
[<img align="left" alt="capturecoder | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />][instagram]

<br />

### Languages and Tools:

<img align="left" alt="C" width="26px" src="https://img.icons8.com/color/48/000000/c-programming.png" />

<img align="left" alt="HTML5" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/html/html.png" />

<img align="left" alt="CSS3" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/css/css.png" />

<img align="left" alt="Javascript" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/javascript/javascript.png">

<img align="left" alt="Android" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/android/android.png" />

<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />

<img align="left" alt="Git" width="26px" src="https://img.icons8.com/color/48/000000/git.png" />

<img align="left" alt="GitHub" width="26px" src="https://raw.githubusercontent.com/github/explore/78df643247d429f6cc873026c0622819ad797942/topics/github/github.png" />

<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/d92924b1d925bb134e308bd29c9de6c302ed3beb/topics/terminal/terminal.png" />

<img align="left" alt="Linux" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/linux/linux.png">

<br />

### Github Stats Top Languages
<a href="https://github.com/capturecoder/github-readme-stats"><img alt="Nitish's Github Stats" src="https://github-readme-stats.vercel.app/api?username=capturecoder&show_icons=true&count_private=true&theme=react&hide_border=true&bg_color=0D1117" /></a>
  <a href="https://github.com/capturecoder/github-readme-stats"><img alt="Nitish's Top Languages" src="https://github-readme-stats.vercel.app/api/top-langs/?username=capturecoder&langs_count=8&layout=compact&theme=react&hide_border=true&bg_color=0D1117" /></a>
 

### Activity Tab
<p align="center"> <img alt="Nitish's Activity Tab" src="https://activity-graph.herokuapp.com/graph?username=capturecoder&theme=xcode" /></p>

### Commit History

[![GitHub Streak](http://github-readme-streak-stats.herokuapp.com?user=capturecoder&theme=dark&hide_border=true)](https://github.com/DenverCoder1/github-readme-streak-stats)



[website]: https://capturecoder.github.io/
[facebook]: https://fb.me/capturecoder
[twitter]: https://twitter.com/capturecoder
[youtube]: https://youtube.com/capturecoder
[instagram]: https://instagram.com/capturecoder
[linkedin]: https://linkedin.com/in/capturecoder
